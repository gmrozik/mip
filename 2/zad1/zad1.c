#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <quantum.h>
#include <math.h>

#define pi 4 * atan(1)

void quantum_r_y_hermitian(int target, float gamma, quantum_reg *reg)
{
  quantum_matrix m;
  m = quantum_new_matrix(2, 2);

  //hermitian - zamiana + z - przy sinusach
  m.t[0] = cos(gamma / 2);  m.t[1] = sin(gamma / 2);
  m.t[2] = -sin(gamma / 2);  m.t[3] = cos(gamma / 2);

  quantum_gate1(target, m, reg);
  quantum_delete_matrix(&m);
}

void quantum_chadamard(int control, int target, quantum_reg *reg)
{
	quantum_r_y(target,pi/4,reg);
	quantum_cnot(control,target,reg);
	quantum_r_y_hermitian(target,pi/4,reg);
}

void quantum_r_gate(int target, quantum_reg *reg)
{
	quantum_matrix m;	
	m=quantum_new_matrix(2,2);  
  
    m.t[0]=sqrt(2.0/3);
	m.t[1]=-sqrt(1.0/3);
	m.t[2]=sqrt(1.0/3);
	m.t[3]=sqrt(2.0/3);

	quantum_gate1(target,m,reg);
	quantum_delete_matrix(&m);
}


int main ()
{
	quantum_reg reg;

	srand(time(0));

	reg = quantum_new_qureg(0, 2);
	quantum_r_gate(0,&reg);
   	quantum_hadamard(1,&reg);
	quantum_chadamard(0,1,&reg);

	int result[4];
    int i=0;
    result[0] = 0;
    result[1] = 0;
    result[2] = 0;
    result[3] = 0;

	for(;i<1000;i++){
		result[quantum_measure(reg)]++;
	  }

  printf("0 %i\n", result[0]);
  printf("1 %i\n", result[1]);
  printf("2 %i\n", result[2]);
  printf("3 %i\n", result[3]);
	return 0;
}


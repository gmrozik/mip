#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <quantum.h>
#include <math.h>


void uf(quantum_reg *reg)
{
	//11010
	int a = 26;

	if(a&1)
		quantum_cnot(1,0,reg);

	if(a&2)
		quantum_cnot(2,0,reg);
	
	if(a&4)
		quantum_cnot(3,0,reg);

	if(a&8)
		quantum_cnot(4,0,reg);

	if(a&16)
		quantum_cnot(5,0,reg);
}

void bernstein_problem()
{
	quantum_reg reg;
	reg = quantum_new_qureg(1,6);
	int i;
	
	for(i=0;i<6;i++)
	{
		quantum_hadamard(i,&reg);
	}

	uf(&reg);

	for(i=0;i<6;i++)
	{
		quantum_hadamard(i,&reg);
	}

	printf("%lld \n",quantum_measure(reg)>>1);
}


int main ()
{
	quantum_reg reg;
	srand(time(0));

	printf("a == 26 (11010) \n");

	printf("test for x=22 (10110) \n");
	//x=10110 y=0
	reg = quantum_new_qureg(22<<1, 6);
	uf(&reg);
	printf("%lld\n", quantum_measure(reg) & 1);

	printf("test for x=30 (11110) \n");
	//x=11110 y=0
	reg = quantum_new_qureg(30<<1, 6);
	uf(&reg);
	printf("%lld\n", quantum_measure(reg) & 1);

	
	printf("classic pc test for a==26\n");
	
	int i=0;
	int sum=0;
	
	for(;i<5;i++)
	{
		reg = quantum_new_qureg(1<<(i+1),6);
		uf(&reg);
		sum+= (quantum_measure(reg) & 1) <<i;
	}

	printf("result: %d\n",sum);


	printf("bernstein result: \n");
	bernstein_problem();
	return 0;
}


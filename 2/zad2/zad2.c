#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <quantum.h>
#include <math.h>


void u_f0(quantum_reg *reg)
{
}

void u_f1(quantum_reg *reg)
{
	quantum_cnot(1,0,reg);
}

void quantum_flip(int target, quantum_reg *reg)
{
	quantum_matrix m;
    m = quantum_new_matrix(2, 2);

    m.t[0] = 0;  m.t[1] = 1; 
    m.t[2] = 1;  m.t[3] = 0;
  
    quantum_gate1(target, m, reg);
    quantum_delete_matrix(&m);
}
void u_f2(quantum_reg *reg)
{
	quantum_cnot(1,0,reg);
	quantum_flip(0,reg);
}

void u_f3(quantum_reg *reg)
{
	quantum_flip(0,reg);
}


void deutsch_problem(quantum_reg *reg,void (*u_f)(quantum_reg*))
{
	quantum_flip(0,reg);
	quantum_flip(1,reg);

	quantum_hadamard(0,reg);
	quantum_hadamard(1,reg);

	u_f(reg);

	//hadamard na input
	quantum_hadamard(1,reg);

}

int main ()
{
	quantum_reg reg;
	srand(time(0));


	reg = quantum_new_qureg(0, 2);
    u_f0(&reg);
	printf("f0(00) == %lld \n",quantum_measure(reg));

	reg = quantum_new_qureg(2, 2);
    u_f0(&reg);
	printf("f0(01) == %lld \n",quantum_measure(reg));

	printf("\n");

	reg = quantum_new_qureg(0, 2);
    u_f1(&reg);
	printf("f1(00) == %lld \n",quantum_measure(reg));

	reg = quantum_new_qureg(2, 2);
    u_f1(&reg);
	printf("f1(01) == %lld \n",quantum_measure(reg));

	printf("\n");

	
	reg = quantum_new_qureg(0, 2);
    u_f2(&reg);
	printf("f2(00) == %lld \n",quantum_measure(reg));

	reg = quantum_new_qureg(2, 2);
    u_f2(&reg);
	printf("f2(01) == %lld \n",quantum_measure(reg));

	printf("\n");

	reg = quantum_new_qureg(0, 2);
    u_f3(&reg);
	printf("f3(00) == %lld \n",quantum_measure(reg));

	reg = quantum_new_qureg(2, 2);
    u_f3(&reg);
	printf("f3(01) == %lld \n",quantum_measure(reg));

	printf("\n");






	reg = quantum_new_qureg(0, 2);
    deutsch_problem(&reg,u_f0);
	printf("f0 dp == %s \n",quantum_measure(reg)>1?"constant":"non-constant");

	reg = quantum_new_qureg(0, 2);
    deutsch_problem(&reg,u_f1);
	printf("f1 dp == %s\n",quantum_measure(reg)>1?"constant":"non-constant");

	reg = quantum_new_qureg(0, 2);
    deutsch_problem(&reg,u_f2);
	printf("f2 dp == %s \n",quantum_measure(reg)>1?"constant":"non-constant");

	reg = quantum_new_qureg(0, 2);
    deutsch_problem(&reg,u_f3);
	printf("f3 dp == %s\n",quantum_measure(reg)>1?"constant":"non-constant");

	return 0;
}


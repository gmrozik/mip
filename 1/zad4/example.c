#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <quantum.h>

int main ()
{
  quantum_reg reg;
  quantum_reg reg2;

  int res;
  
  srand(time(0));
  reg = quantum_new_qureg(1, 2);
  reg2 = quantum_new_qureg(2, 2);

  quantum_cnot(1,0,&reg);
  quantum_cnot(0,1,&reg);
  quantum_cnot(1,0,&reg);
  res = quantum_measure(reg);
  printf("2 == %d\n", res);


  quantum_cnot(1,0,&reg2);
  quantum_cnot(0,1,&reg2);
  quantum_cnot(1,0,&reg2);
  res = quantum_measure(reg2);
  printf("1 == %d\n", res);
  
  return 0;
}

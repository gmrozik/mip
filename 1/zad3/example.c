#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <quantum.h>

int main ()
{
  quantum_reg reg;

  int result[4];
  int i=0;
  result[0] = 0;
  result[1] = 0;
  result[2] = 0;
  result[3] = 0;
  
  srand(time(0));
  reg = quantum_new_qureg(0, 2);

  quantum_hadamard(0, &reg);
  quantum_cnot(0,1,&reg);
  
  for(;i<10000;i++){
    result[quantum_measure(reg)]++;
  }
  
  printf("0 %i\n", result[0]);
  printf("1 %i\n", result[1]);
  printf("2 %i\n", result[2]);
  printf("3 %i\n", result[3]);
     
  printf("%f\n", (double)result[0]/((double)result[0]+result[1]));
  printf("%f\n", (double)result[1]/((double)result[0]+result[1]));
     
  printf("%f\n", (double)result[2]/((double)result[2]+result[3]));
  printf("%f\n", (double)result[3]/((double)result[2]+result[3]));

  return 0;
}

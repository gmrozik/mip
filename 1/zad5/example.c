#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <quantum.h>

int main ()
{
  quantum_reg reg;

  int result;
  int i=0;
  
  srand(time(0));
  for(;i<4;i++){
    reg = quantum_new_qureg(i, 2);
    quantum_toffoli(0,1,2,&reg);
    result = quantum_bmeasure(2,&reg);
    printf("%i and %i = %i\n", i/2, i%2 , result);
  }

  return 0;
}
